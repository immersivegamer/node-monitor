﻿using System;
using CommandLine;

namespace NodeMonitor
{
    [Verb("monitor", HelpText = "Start a monitor to ping nodes.")]
    public class MonitorOptions
    {
        //commit options here
        [Option('s', "secret")]
        public string Secret { get; internal set; }
        [Option('h', "hosts", Default = "./hosts.txt")]
        public string HostListFile { get; internal set; }

        [Option('t', "timeout", Default = 1000)]
        public int Timeout { get; internal set; }
    }

    [Verb("node", HelpText = "Start a node to be monitored.")]
    public class NodeOptions
    {
        //normal options here
        [Option('n', "name")]
        public string ServerId { get; internal set; }
        [Option('s', "secret")]
        public string Secret { get; internal set; }
        [Option('h', "host", Default = "localhost")]
        public string Host { get; internal set; }
        [Option('p', "port", Default = 50051)]
        public int Port { get; internal set; }
    }
}
