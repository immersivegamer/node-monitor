using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Grpc.Core;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace NodeMonitor
{
    public class WorkerMonitor : BackgroundService
    {
        public static MonitorOptions Config;
        private readonly ILogger<WorkerMonitor> _logger;
        private readonly IHostApplicationLifetime applicationLifetime;
        string[] hosts = new string[0];
        Dictionary<string, Node.NodeClient> clients = new Dictionary<string, Node.NodeClient>();

        public WorkerMonitor(ILogger<WorkerMonitor> logger, IHostApplicationLifetime applicationLifetime)
        {
            _logger = logger;
            this.applicationLifetime = applicationLifetime;
            clients = InitializeClients(HostNodeManager.Instance);
        }

        private Dictionary<string, Node.NodeClient> InitializeClients(HostNodeManager manager)
        {
            var map = new Dictionary<string, Node.NodeClient>();

            manager.LoadFromPath(Config.HostListFile);
            foreach(var host in manager.Hosts)
            {
                var channel = new Channel(host.Hostname, ChannelCredentials.Insecure);
                var client = new Node.NodeClient(channel);
                map.Add(host.Uri, client);    
            }

            return map;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {

            while (!stoppingToken.IsCancellationRequested)
            {
                try
                {
                   await Action(stoppingToken);
                }
                catch(Exception ex)
                {
                    _logger.LogError(ex, "Unhandled error, shutting down.");
                    applicationLifetime.StopApplication();
                }

                await Task.Delay(1000, stoppingToken);
            }
        }

        private async Task Action(CancellationToken stoppingToken)
        {
            var request = new PingRequest { Key = Config.Secret };

            foreach (var host in HostNodeManager.Instance.Hosts)
            {
                try
                {
                    var reply = await clients[host.Uri].PingAsync(request, deadline: GetDeadlineFromNow());
                    host.LastPingTime = DateTime.UtcNow;
                    _logger.LogInformation("Action: {action}, Host: {uri}, Message: {message}", "Ping", host.Uri, reply.Message);

                    if (stoppingToken.IsCancellationRequested)
                    {
                        return;
                    }
                }
                catch (Grpc.Core.RpcException timeout)
                {
                    _logger.LogWarning("Host: {uri}, Client error (timeout?), message: {msg}", host.Uri, timeout.Message);
                }
            }

            HostNodeManager.Instance.OutputHtmlReport();
        }

        private DateTime? GetDeadlineFromNow()
        {
            if (Config.Timeout <= 0)
                return null;

            return DateTime.UtcNow.AddMilliseconds(Config.Timeout);
        }
    }
}
