using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Grpc.Core;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace NodeMonitor
{
    public class WorkerNode : BackgroundService
    {
        public static NodeOptions Config;

        private readonly ILogger<WorkerNode> _logger;

        public WorkerNode(ILogger<WorkerNode> logger)
        {
            _logger = logger;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            Server server = new Server
            {
                Services = { Node.BindService(new NodeService(Config)) },
                Ports = { new ServerPort(Config.Host, Config.Port, ServerCredentials.Insecure) }
            };

            server.Start();

            _logger.LogInformation($"Node server listening on {Config.Host}:{Config.Port}");

            while (!stoppingToken.IsCancellationRequested)
            {
                await Task.Delay(1000, stoppingToken);
            }

            server.ShutdownAsync().Wait();
        }
    }
}
