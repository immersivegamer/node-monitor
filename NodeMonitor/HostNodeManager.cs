﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml.Linq;

namespace NodeMonitor
{
    public class HostNode
    {
        public string Group { get; set; }
        public string Name { get; set; }
        public string Hostname { get; set; }
        public DateTime LastPingTime { get; set; }

        public string Uri => $"{Group}/{Name}/{Hostname}";
    }

    public class HostNodeManager
    {
        private static HostNodeManager _instance;
        public static HostNodeManager Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new HostNodeManager();
                }
                return _instance;
            }
        }

        private HostNodeManager() { }

        public List<HostNode> Hosts = new List<HostNode>();

        public void OutputHtmlReport(string filepath = "./report.html", string templatePath = "./template.html")
        {
            var root = new XElement("hosts");
            foreach(var host in Hosts)
            {
                root.Add(new XElement("host",
                    new XElement("g", host.Group),
                    new XElement("n", host.Name),
                    new XElement("h", host.Hostname),
                    new XElement("p", host.LastPingTime)
                ));
            }

            var dynamicHtml = root.ToString();

            var templateHtml = System.IO.File.Exists(templatePath) ? System.IO.File.ReadAllText(templatePath) : null;
            if(!string.IsNullOrWhiteSpace(templatePath) && templatePath.Contains("{{data}}"))
            {
                templateHtml = templateHtml.Replace("{{data}}", dynamicHtml);
            }
            else
            {
                templateHtml = dynamicHtml;
            }

            System.IO.File.WriteAllText(filepath, templateHtml);
        }

        public void LoadFromPath(string filepath)
        {
            Hosts = new List<HostNode>();

            Regex groupPattern = new Regex(@"\[(.*?)\]");
            Regex hostPattern = new Regex(@"(\w+)/(.*)");

            string currentGroup = string.Empty;

            var lines = System.IO.File.ReadAllLines(filepath);
            foreach(var line in lines)
            {
                if (string.IsNullOrWhiteSpace(line))
                    continue;

                if (groupPattern.IsMatch(line))
                {
                    currentGroup = groupPattern.Match(line).Groups[1].Value;
                    continue;
                }

                if (hostPattern.IsMatch(line))
                {
                    var matches = hostPattern.Match(line);
                    Hosts.Add(
                        new HostNode
                        {
                            Group = currentGroup,
                            Name = matches.Groups[1].Value,
                            Hostname = matches.Groups[2].Value,
                        }
                    );
                    
                    continue;
                }
            }
        }
    }
}
