﻿using System.Threading.Tasks;
using Grpc.Core;

namespace NodeMonitor
{

        public class NodeService : Node.NodeBase
        {
            private readonly NodeOptions config;

            public NodeService(NodeOptions config)
            {
                this.config = config;
            }

            public override Task<PingReply> Ping(PingRequest request, ServerCallContext context)
            {
                var reply = new PingReply
                {
                    Message = $"Online - Server: {config.ServerId}"
                };


                if (request.Key != config.Secret)
                {
                    reply.Message = "UNAUTHORIZED";
                }

                return Task.FromResult(reply);
            }
        }
    
}
