using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using CommandLine;

namespace NodeMonitor
{
    public partial class Program
    {
        static string[] args;
        public static int Main(string[] args)
        {
            Program.args = args;
            return CommandLine.Parser.Default.ParseArguments<NodeOptions, MonitorOptions>(args)
            .MapResult(
              (NodeOptions opts) => RunNode(opts),
              (MonitorOptions opts) => RunMonitor(opts),
              errs => 1);
        }

        private static int RunMonitor(MonitorOptions opts)
        {
            WorkerMonitor.Config = opts;
            IHostBuilder builder = Host.CreateDefaultBuilder(args)
                .ConfigureServices((hostContext, services) =>
                {
                    services.AddHostedService<WorkerMonitor>();
                });

            builder.Build().Run();
            return 0;
        }

        private static int RunNode(NodeOptions opts)
        {
            WorkerNode.Config = opts;
            IHostBuilder builder = Host.CreateDefaultBuilder(args)
                .ConfigureServices((hostContext, services) =>
                {
                    services.AddHostedService<WorkerNode>();
                });

            builder.Build().Run();
            return 0;
        }
            
    }
}
