﻿using CommandLine;
using Grpc.Core;
using System;
using System.Threading.Tasks;

namespace node
{
    class Program
    {

        

        static int Main(string[] args)
        {
            // node (server) or monitor (client)?
            return Parser.Default.ParseArguments<NodeOptions, MonitorOptions>(args)
              .MapResult(
                (NodeOptions opts) => RunNode(opts),
                (MonitorOptions opts) => RunMonitor(opts),
                errs => 1);
        }

        private static int RunMonitor(MonitorOptions opts)
        {
            throw new NotImplementedException();
        }

        private static int RunNode(NodeOptions opts)
        {
            throw new NotImplementedException();
        }
    }
}
